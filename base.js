class Fibonacci {
    constructor(level, value) {
    this.level 	= level;
    this.value 	= value;
    }
}

const f1 = new Fibonacci (1, 1);
const f2 = new Fibonacci (2, 1);
const f3 = new Fibonacci (3, 2);
const f4 = new Fibonacci (4, 3);
const f5 = new Fibonacci (5, 5);
const f6 = new Fibonacci (6, 8);
const f7 = new Fibonacci (7, 13);
const f8 = new Fibonacci (8, 21);
const f9 = new Fibonacci (9, 34);
const f10 = new Fibonacci (10, 55);
const f11 = new Fibonacci (11, 89);
const f12 = new Fibonacci (12, 144);

const fibonacciArray = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12];


class Harmonic {
    constructor(level, value) {
    this.level 	= level;
    this.value 	= value;
    }
}
const h0 = new Harmonic (0, 1);
const h1 = new Harmonic (1, 1);
const h2 = new Harmonic (2, 1.059);
const h3 = new Harmonic (3, 1.122);
const h4 = new Harmonic (4, 1.189);
const h5 = new Harmonic (5, 1.260);
const h6 = new Harmonic (6, 1.335);
const h7 = new Harmonic (7, 1.414);
const h8 = new Harmonic (8, 1.498);
const h9 = new Harmonic (9, 1.587);
const h10 = new Harmonic (10, 1.682);
const h11 = new Harmonic (11, 1.782);
const h12 = new Harmonic (12, 1.888);
const h13 = new Harmonic (13, 2.0);

const harmonicArray = [h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13];

class ratio {
    constructor(level, value) {
    this.level 	= level;
    this.value 	= value;
    }
}

const r0 = new ratio (0, 10);
const r1 = new ratio (1, 10);
const r2 = new ratio (2, 20);
const r3 = new ratio (3, 30);
const r4 = new ratio (4, 50);
const r5 = new ratio (5, 80);
const r6 = new ratio (6, 130);
const r7 = new ratio (7, 210);
const r8 = new ratio (8, 340);
const r9 = new ratio (9, 550);
const r10 = new ratio (10, 890);
const r11 = new ratio (11, 1440);
const r12 = new ratio (12, 2330);
const r13 = new ratio (13, 3770);


const ratioArray = [r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13];

class Item {
    constructor(name, level, force, step, points, score) {
        this.name 	= name;
        this.level 	= level;
        this.force  = force;
        this.step 	= step;
        this.points = points; // current points based on level step
        this.score 	= score; //total score
    }
}
// Item 1
const item1 = new Item ('item1', 0, 1, 1, 0, 0);

let itemLvl = document.querySelector('#itemLvl');
let info = document.querySelector('#info');
let itemPoints = document.querySelector('#itemPoints');
let itemScore = document.querySelector('#itemScore');
let push = document.querySelector('#push');

push.addEventListener('click', () => {
    // if (item1.level == 12 ) {
    if (item1.level < 12) {
        item1.level++;
        item1.score += item1.points;
        item1.points = 0;
        itemLvl.innerHTML = item1.level;
        itemPoints.innerHTML = item1.points.toFixed(3);
        itemScore.innerHTML = item1.score.toFixed(3);
        info.innerHTML = "Progress...";
        setInterval(() => {
            lvlIncr();
        }, 20);
    } 
    // else if (item1.level == 12) {
    //     push.innerHTML = "have to d-none the button";
    // }
    else {
        info.innerHTML = "Item1 completed !";
        item1.score += item1.points;
        itemScore.innerHTML = item1.score.toFixed(3);
    }

});

function lvlIncr() {
    if (item1.level < 13) {
        if (item1.level == 12) {
                push.innerHTML = "have to d-none the button";
        }
        if (item1.points < ratioArray[item1.level].value) {
            item1.points += harmonicArray[item1.level].value;
            console.log("item1.points: " + item1.points);
            itemPoints.innerHTML = item1.points.toFixed(3);
        } else {
            info.innerHTML = "Level " + item1.level + " completed !";
            itemScore.innerHTML = item1.score.toFixed(3);
        } 
    }  else {  
        info.innerHTML = "item1 completed !";
        item1.score += item1.points;
        itemScore.innerHTML = item1.score.toFixed(3);
    }     
          
}

setInterval(() => {
    lvlIncr();
}, 20);


// simple increment
const countElement = document.querySelector('#count');
let count = parseInt(countElement.innerHTML);

function increment() {
    count++;
    countElement.innerHTML = count;
}

let intervalTime = 5000;
let intervalId = setInterval(lvlIncr, intervalTime);

intervalId = setInterval(lvlIncr, intervalTime);

setInterval(() => {
    increment();
}, 1000);


